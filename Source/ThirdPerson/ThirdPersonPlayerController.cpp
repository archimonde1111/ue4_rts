// Copyright Epic Games, Inc. All Rights Reserved.

#include "ThirdPersonPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "ThirdPersonCharacter.h"
#include "Engine/World.h"
#include "Engine/GameViewportClient.h"
#include "BaseUnit.h"

AThirdPersonPlayerController::AThirdPersonPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void AThirdPersonPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	float MouseLocationInViewportX(0.f);
	float MouseLocationInViewportY(0.f);
	GetMousePosition(MouseLocationInViewportX, MouseLocationInViewportY);
	int32 ViewportSizeX = 0;
	int32 ViewportSizeY = 0;
	GetViewportSize(ViewportSizeX, ViewportSizeY);

	FVector2D MouseViewportPosition(MouseLocationInViewportX/ViewportSizeX, MouseLocationInViewportY/ViewportSizeY);
	Move_Pawn_X_Direction(MouseViewportPosition.X);
	Move_Pawn_Y_Direction(MouseViewportPosition.Y);

}
void AThirdPersonPlayerController::Move_Pawn_X_Direction(float MouseViewportXPosition)
{
	if(MouseViewportXPosition <= 0.025f)
	{
		APawn* OwnedPawn = GetPawn();
		if(OwnedPawn)
		{
			FVector VectorToAddToLocalPosition(FVector(0.f, -(CameraMovingSpeed * GetWorld()->DeltaTimeSeconds), 0.f));
			OwnedPawn->AddActorLocalOffset(VectorToAddToLocalPosition);
		}
	}
	else if(MouseViewportXPosition >= 0.975f)
	{
		APawn* OwnedPawn = GetPawn();
		if(OwnedPawn)
		{
			FVector VectorToAddToLocalPosition(FVector(0.f, (CameraMovingSpeed * GetWorld()->DeltaTimeSeconds), 0.f));
			OwnedPawn->AddActorLocalOffset(VectorToAddToLocalPosition);
		}
	}
}
void AThirdPersonPlayerController::Move_Pawn_Y_Direction(float MouseViewportYPosition)
{
	if(MouseViewportYPosition <= 0.025f)
	{
		APawn* OwnedPawn = GetPawn();
		if(OwnedPawn)
		{
			FVector VectorToAddToLocalPosition(FVector((CameraMovingSpeed * GetWorld()->DeltaTimeSeconds), 0.f, 0.f));
			OwnedPawn->AddActorLocalOffset(VectorToAddToLocalPosition);
		}
	}
	else if(MouseViewportYPosition >= 0.975f)
	{
		APawn* OwnedPawn = GetPawn();
		if(OwnedPawn)
		{
			FVector VectorToAddToLocalPosition(FVector(-(CameraMovingSpeed * GetWorld()->DeltaTimeSeconds), 0.f, 0.f));
			OwnedPawn->AddActorLocalOffset(VectorToAddToLocalPosition);
		}
	}
}


void AThirdPersonPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	SetInputMode(FInputModeGameAndUI::FInputModeGameAndUI());

	InputComponent->BindAction("Select", IE_Pressed, this, &AThirdPersonPlayerController::Select);
	InputComponent->BindAction("GiveCommand", IE_Pressed, this, &AThirdPersonPlayerController::Give_Command);
}
void AThirdPersonPlayerController::Select()
{
	FHitResult HitResult;
	GetHitResultUnderCursor(ECC_Visibility, false, HitResult);
	
	if(HitResult.bBlockingHit)
	{
		ABaseUnit* SelectedActor = Cast<ABaseUnit> (HitResult.Actor);
		if(SelectedActor && SelectedActor->Get_Unit_Nation() == ENation::Player)
		{
			SelectedActor->Highlight_Unit(true);
			SelectedUnits.AddUnique(SelectedActor);
		}
		else
		{
			for(ABaseUnit* Unit : SelectedUnits)
			{
				Unit->Highlight_Unit(false);
			}
			SelectedUnits.Empty();
		}
	}
}

void AThirdPersonPlayerController::Give_Command()
{
	if(SelectedUnits.Num() > 0)
	{
		FHitResult HitResult;
		GetHitResultUnderCursor(ECC_Visibility, false, HitResult);

		if (HitResult.bBlockingHit)
		{
			for(ABaseUnit* PlayerUnit : SelectedUnits)
			{
				Set_New_Move_Destination(HitResult, PlayerUnit);
			}
		}
	}
}
void AThirdPersonPlayerController::Set_New_Move_Destination(const FHitResult HitResult, ABaseUnit* PlayerUnit)
{
	if (PlayerUnit && PlayerUnit->GetController())
	{
		ABaseUnit* TargetUnit = Cast<ABaseUnit> (HitResult.Actor);
		if(TargetUnit && TargetUnit->Get_Unit_Nation() != ENation::Player)
		{
			PlayerUnit->Set_Attack_Target_As(TargetUnit);
		}
		else
		{
			FVector DestLocation = HitResult.ImpactPoint;
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(PlayerUnit->GetController(), DestLocation);
			PlayerUnit->Remove_Attack_Target();
		}
	}
}
