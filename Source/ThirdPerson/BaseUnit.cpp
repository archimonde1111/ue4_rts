// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseUnit.h"
#include "Components/WidgetComponent.h"
#include "HighlightWidgetComponent.h"
#include "Components/CapsuleComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Kismet/GameplayStatics.h"

ABaseUnit::ABaseUnit()
{
	PrimaryActorTick.bCanEverTick = true;

	Highlight = CreateDefaultSubobject<UHighlightWidgetComponent> (FName("Highlight"));
	Highlight->SetupAttachment(GetRootComponent());

	SelectionComponent = CreateDefaultSubobject<USelectionComponent> (FName("SelectionComponent"));
	SelectionComponent->Set_Unit_Nation(ENation::Player);
}

void ABaseUnit::BeginPlay()
{
	Super::BeginPlay();
	Highlight->SetVisibility(false);

	CurrentHealth = Health;
}

void ABaseUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(!CanAttack)
	{
		WaitedTime += DeltaTime;
		if(WaitedTime >= TimeToWaitBetweenAttacks)
		{
			CanAttack = true;
			WaitedTime = 0;
		}
	}

	if(bAttackCommand && UnitToAttack.IsValid())
	{
		float DistanceToTarget = GetDistanceTo(UnitToAttack.Get());
		if(DistanceToTarget >= AttackRange)
		{
			UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), UnitToAttack.Get());
		}
		else if(CanAttack)
		{
			bIsAttacking = true;
			UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), this);
			UGameplayStatics::ApplyDamage(UnitToAttack.Get(), Damage, GetController(), this, UDamageType::StaticClass());
			CanAttack = false;
		}
		else
		{
			UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), this);
		}

	}
	else
	{
		Remove_Attack_Target();
	}
}

float ABaseUnit::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	CurrentHealth -= DamageAmount;

	if(Is_Dead())
	{
		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	return DamageAmount;
}


void ABaseUnit::Highlight_Unit(bool bHighlight)
{
	Highlight->SetVisibility(bHighlight);
}

void ABaseUnit::Set_Attack_Target_As(ABaseUnit* TargetUnit)
{
	bAttackCommand = true;
	UnitToAttack = TargetUnit;
}
void ABaseUnit::Remove_Attack_Target()
{
	bAttackCommand = false;
	UnitToAttack.Reset();
}