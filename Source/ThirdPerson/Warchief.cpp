// Fill out your copyright notice in the Description page of Project Settings.


#include "Warchief.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"


AWarchief::AWarchief()
{
	PrimaryActorTick.bCanEverTick = true;

	Weapon = CreateDefaultSubobject<UStaticMeshComponent> (FName("Weapon"));
	Weapon->SetupAttachment(GetMesh(), TEXT("Weapon"));
	Weapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	GetCapsuleComponent()->SetCapsuleSize(45.f, 115.f);
}

void AWarchief::BeginPlay()
{
	Super::BeginPlay();
}

void AWarchief::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
