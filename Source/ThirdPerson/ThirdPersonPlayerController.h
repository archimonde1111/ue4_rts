// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ThirdPersonPlayerController.generated.h"

class ABaseUnit;

UCLASS()
class AThirdPersonPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AThirdPersonPlayerController();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	UPROPERTY(EditAnywhere, Category = "Camera")
	float CameraMovingSpeed = 500.f;

	TArray<ABaseUnit*> SelectedUnits;

protected:
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	void Select();
	void Give_Command();
	void Set_New_Move_Destination(const FHitResult HitResult, ABaseUnit* Unit);

	void Move_Pawn_X_Direction(float MouseViewportXPosition);
	void Move_Pawn_Y_Direction(float MouseViewportYPosition);

};


