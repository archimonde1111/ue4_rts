// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SelectionComponent.h"
#include "BaseUnit.generated.h"

class UHighlightWidgetComponent;
class USelectionComponent;
class USphereComponent;

UCLASS()
class THIRDPERSON_API ABaseUnit : public ACharacter
{
	GENERATED_BODY()

	bool bAttackCommand = false;
	UPROPERTY()
		TWeakObjectPtr<ABaseUnit> UnitToAttack = nullptr;

public:
	ABaseUnit();

	UPROPERTY(BlueprintReadWrite, Category = "BaseUnit")
	bool bIsAttacking = false;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Highlight")
		UHighlightWidgetComponent* Highlight = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USelectionComponent* SelectionComponent = nullptr;
	
	UPROPERTY(EditAnywhere, Category = "Fight")
		float Health = 100.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Fight")
		float CurrentHealth = 0.f;
	UPROPERTY(EditAnywhere, Category = "Fight")
		float AttackRange = 200.f;
	UPROPERTY(EditAnywhere, Category = "Fight")
		float Damage = 50.f;
	UPROPERTY(EditAnywhere, Category = "Fight")
		float TimeToWaitBetweenAttacks = 1.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Fight")
		float WaitedTime = 0.f;
	bool CanAttack = true;

	
public:	
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE ENation Get_Unit_Nation() const { return SelectionComponent->Get_Unit_Nation(); }
	void Highlight_Unit(bool bHighlight);

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	virtual void Set_Attack_Target_As(ABaseUnit* TargetUnit);
	virtual void Remove_Attack_Target();

	UFUNCTION(BlueprintCallable, Category = "BaseUnit")
		FORCEINLINE float Get_Health_Percent() const { return CurrentHealth/Health; }
	UFUNCTION(BlueprintCallable, Category = "BaseUnit")
		FORCEINLINE bool Is_Dead() const { return CurrentHealth <= 0; }
};
