// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseUnit.h"
#include "Warchief.generated.h"

class UStaticMeshComponent;

UCLASS()
class THIRDPERSON_API AWarchief : public ABaseUnit
{
	GENERATED_BODY()

public:
	AWarchief();

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		UStaticMeshComponent* Weapon = nullptr;

public:	
	virtual void Tick(float DeltaTime) override;

};
