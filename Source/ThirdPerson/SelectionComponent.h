// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SelectionComponent.generated.h"


UENUM()
enum class ENation : uint8
{
	Player = 0,
	Neutral = 1,
	People = 2,
	Orcs = 3
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THIRDPERSON_API USelectionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USelectionComponent();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	ENation UnitNation = ENation::Neutral;

public:	

	FORCEINLINE void Set_Unit_Nation(ENation Nation) { UnitNation = Nation; }
	FORCEINLINE ENation Get_Unit_Nation() const { return UnitNation; }

};
