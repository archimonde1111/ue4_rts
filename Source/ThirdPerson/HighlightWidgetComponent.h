// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "HighlightWidgetComponent.generated.h"

/**
 * 
 */
UCLASS()
class THIRDPERSON_API UHighlightWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()
	
	UPROPERTY()
		UUserWidget* Highlight = nullptr;
};
